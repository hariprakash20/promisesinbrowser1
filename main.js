const {getUsers, getPromiseArray, get7Tasks} = require('./api')

window.addEventListener('load',()=>{
    getUsers().then(respsonse => respsonse.json())
    .then(users => { return Promise.all([getPromiseArray(users),users])} )
    .then(([responseArray, users]) => { 
        data = Promise.all(responseArray)
        return Promise.all([data, users]);
    }) 
    .then(([responseArray,users]) => {
        data = Promise.all(responseArray.map(response=>response.json()))
        return Promise.all([data, users])
    })
    .then(([tasks,users])=>{ tasks.map(response => get7Tasks(response , users))})
    .catch(err =>{
        console.error("error occured in: "+ err.message);
        console.error(err.stack);
    })
})

// 