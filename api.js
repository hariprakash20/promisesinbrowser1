const getUsers = () => fetch("https://jsonplaceholder.typicode.com/users");

const getPromiseArray = users => users.map(user => fetch("https://jsonplaceholder.typicode.com/todos?userId="+user.id))

let mainContainer = document.querySelector('.main-container');

const get7Tasks =  (response, users) =>{
     printTasks(response.slice(0,7), users)
} ;

const renderList = (id, completed, title) => {
    return `<div class="task">
    <input type="checkbox" id="${id}" ${completed? 'checked="checked"' : ''}">
    <p class="title"> ${title}</p>
</div>`
}

function printTasks(tasks, users){
    let userName = ""
    users.map(user => {
        if(user.id == tasks[0].userId) 
        userName = user.name;
    })
    userHTML = `<h1>${userName}</h1>`;
    mainContainer.innerHTML += userHTML;
    tasks.map(task=> {
        mainContainer.innerHTML += renderList(task.id, task.completed, task.title);
    });
}

module.exports = {getUsers, getPromiseArray, get7Tasks, renderList, printTasks};